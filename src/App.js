import React, { Component } from 'react';
import './App.css';
import TaskList from './TaskList';
import Tasks from './Tasks';
import LoginForm from './LoginForm';



//var taskList = new Tasks("user");

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      isLogin: true //login status
    }
    this.taskData = new Tasks("user");//temp
  }
  render() {
    if(this.state.isLogin){
      return (
        <div className="App">
        <TaskList taskData = {this.taskData.GetTasks()}/>
        </div>
      );
    }else{
      return (
        <LoginForm />
      );
    }
  }
}

export default App;
