let onetask = [
{
  "id":"0",
  "name":"task one",
  "description":"i task one and you need to work",
  "creation_date":"2018-09-29T09:00:00",
  "priority":"2",
  "state":"WRK",
  "plan_time":"2018-09-30T09:00:00",
  "work_time":"2018-09-29T21:00:00",
  "date_start":"2018-09-29T10:00:00",
  "date_stop":""
},
{
  "id":"1",
  "name":"task one",
  "description":"i task one and you need to work",
  "creation_date":"2018-09-29T09:00:00",
  "priority":"0",
  "state":"REG",
  "plan_time":"2018-09-30T09:00:00",
  "work_time":"0",
  "date_start":"0",
  "date_stop":""
},
{
  "id":"2",
  "name":"task three",
  "description":"i task three and you need to work",
  "creation_date":"2018-09-28T09:00:00",
  "priority":"1",
  "state":"END",
  "plan_time":"2018-09-30T09:00:00",
  "work_time":"2018-09-30T09:00:00",
  "date_start":"2018-09-30T11:02:10",
  "date_stop":"2018-09-30T20:30:30"
},
{
  "id":"3",
  "name":"task three",
  "description":"i task three and you need to work",
  "creation_date":"2018-09-28T09:00:00",
  "priority":"0",
  "state":"END",
  "plan_time":"2018-09-30T09:00:00",
  "work_time":"2018-09-30T09:00:00",
  "date_start":"2018-09-30T11:02:10",
  "date_stop":"2018-09-30T20:30:30"
},
{
  "id":"4",
  "name":"task three",
  "description":"i task three and you need to work",
  "creation_date":"2018-09-28T09:00:00",
  "priority":"1",
  "state":"REG",
  "plan_time":"2018-09-30T09:00:00",
  "work_time":"2018-09-30T09:00:00",
  "date_start":"2018-09-30T11:02:10",
  "date_stop":"2018-09-30T20:30:30"
},
{
  "id":"5",
  "name":"task gg",
  "description":"i task three and you need to work",
  "creation_date":"2018-09-28T09:00:00",
  "priority":"2",
  "state":"REG",
  "plan_time":"2018-09-30T09:00:00",
  "work_time":"2018-09-30T09:00:00",
  "date_start":"2018-09-30T11:02:10",
  "date_stop":"2018-09-30T20:30:30"
},
{
  "id":"6",
  "name":"task three",
  "description":"i task three and you need to work",
  "creation_date":"2018-09-28T09:00:00",
  "priority":"1",
  "state":"REG",
  "plan_time":"2018-09-30T09:00:00",
  "work_time":"2018-09-30T09:00:00",
  "date_start":"2018-09-30T11:02:10",
  "date_stop":"2018-09-30T20:30:30"
}
]


export default class Tasks {
  constructor(userName) {
    this._user_name = userName;
  }
  GetTasks() {
    return onetask;
  }
}
