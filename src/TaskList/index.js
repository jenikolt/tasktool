import React, { Component } from 'react';
import './style.css';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import Chip from '@material-ui/core/Chip';

function wordPriority(priority_num){
  let n = Number(priority_num);
  if (n < 1){
    return "низкий";
  }else if (n > 1) {
    return "высокий";
  }else {
    return "обычный";
  }
}
const taskStates = {
  REG: "REG",
  TAP: "TAP",
  WRK: "WRK",
  REJ: "REJ",
  END: "END"
}
function wordState(state_code){
  switch (state_code) {
    case taskStates.REG: return "зарегистрирована"; break;
    case taskStates.TAP: return "принято/пауза"; break;
    case taskStates.WRK: return "в работе"; break;
    case taskStates.REJ: return "отклонена"; break;
    case taskStates.END: return "завершена"; break;
    default: return "неизвестно";
  }
}
function dateString(datestr){
  return new Date(datestr).toLocaleString();
}
function timeString(timestr){
  return new Date(timestr).toLocaleTimeString();
}
function TableView(taskeElments) {
  const rows = taskeElments.map((row)=>
    <TableRow key = {row.id}>
      <TableCell>{row.id}</TableCell>
      <TableCell>{row.name}</TableCell>
      <TableCell>{row.description}</TableCell>
      <TableCell>{dateString(row.creation_date)}</TableCell>
      <TableCell>{wordPriority(row.priority)}</TableCell>
      <TableCell>{wordState(row.state)}</TableCell>
      <TableCell>{timeString(row.plan_time)}</TableCell>
      <TableCell>{timeString(row.work_time)}</TableCell>
    </TableRow>
  )
  return (
    <Paper className="taskTable">
    <Table>
    <TableHead>
    <TableRow>
      <TableCell># задачи</TableCell>
      <TableCell>Название</TableCell>
      <TableCell>Описание</TableCell>
      <TableCell>Дата создания</TableCell>
      <TableCell>Приоритет</TableCell>
      <TableCell>Статус</TableCell>
      <TableCell>Плановое время</TableCell>
      <TableCell>Фактическое время</TableCell>
    </TableRow>
    </TableHead>
    <TableBody>
    {rows}
    </TableBody>
    </Table>
    </Paper>
  )
}
function TableShortView(taskeElments) {
  const rows = taskeElments.map((row)=>
    <TableRow key = {row.id}>
      <TableCell>{row.name}</TableCell>
      <TableCell>{wordPriority(row.priority)}</TableCell>
      <TableCell>{wordState(row.state)}</TableCell>
    </TableRow>
  )
  return (
    <Paper className="taskTable">
    <Table>
    <TableHead>
    <TableRow>
      <TableCell>Название</TableCell>
      <TableCell>Приоритет</TableCell>
      <TableCell>Статус</TableCell>
    </TableRow>
    </TableHead>
    <TableBody>
    {rows}
    </TableBody>
    </Table>
    </Paper>
  )
}
function ScrumView(taskeElments){
  let plan_tasks = [];
  let work_tasks = [];
  let end_tasks = [];
  taskeElments.forEach(function(item){
    if(item.state === taskStates.END)
    {
      end_tasks.push(item);
    }
    else if (item.state === taskStates.WRK) {
      work_tasks.push(item);
    }else{
      plan_tasks.push(item);
    }
  });
  function comparePriority(itemA, itemB){
    return itemB.priority - itemA.priority;
  }
  plan_tasks.sort(comparePriority);
  work_tasks.sort(comparePriority);
  end_tasks.sort(comparePriority);

  const plan_items = plan_tasks.map((item)=>
    <Card key={item.id} className="scrum_item">
    <CardContent>
    {item.name}|<span>{wordPriority(item.priority)}</span><Divider/>{item.description}
    </CardContent>
    </Card>);
  const work_items = work_tasks.map((item)=>
    <Card key={item.id} className="scrum_item">
    <CardContent>
    {item.name}|<span>{wordPriority(item.priority)}</span><Divider/>{item.description}
    </CardContent>
    </Card>);
  const end_items = end_tasks.map((item)=>
    <Card key={item.id} className="scrum_item">
    <CardContent>
    {item.name}|<span>{wordPriority(item.priority)}</span><Divider/>{item.description}
    </CardContent>
    </Card>);
  return(
    <Paper className="scrum_table">
      <h3>tasks user</h3>
      <div className="scrum_columns">
        <div className="scrum_column">
          <h4>plan</h4>
          {plan_items}
        </div>
        <div className="scrum_column">
          <h4>work</h4>
          {work_items}
        </div>
        <div className="scrum_column">
          <h4>end</h4>
          {end_items}
        </div>
      </div>
    </Paper>
  )
}

function SingleTaskView(taskItem){
  const style = {padding: '6px', width: '100%', margin: '10px'}
  return(
    <Card raised={true} style={style}>
        <h2>{taskItem.name}</h2>
        <p>{taskItem.description}</p>
        <hr />
        <time>{new Date(taskItem.creation_date).toLocaleString("ru")}</time>
        <p>
          <span>Приоритет:</span>{wordPriority(taskItem.priority)}
          <span>|</span>
          <span>Статус:<b>{wordState(taskItem.state)}</b></span>
        </p>
        {new Date(taskItem.plan_time).toTimeString()}
        {new Date(taskItem.work_time).toTimeString()}
    </Card>
  )
}

export default class TaskList extends Component {
  constructor(props){
    super(props);
    this.taskData = this.props.taskData;
    this.SingleTaskItem;
    this.state = {
      //filter
      sortPriorityUP: null,
      viewtype: "table",//default table, single
      taskId: 1
    }
  }
  render(){
    const btnChangeView =
      <div>
        <Button onClick={this.btnChangeViewClick.bind(this,"table")}>Table</Button>
        <Button onClick={this.btnChangeViewClick.bind(this,"scrum")}>Scrum</Button>
        <Button onClick={this.btnChangeViewClick.bind(this,"short")}>Short</Button>
        <Button onClick={this.btnChangeViewClick.bind(this,"single")}>Single</Button>
      </div>;
    const btnSortTable =
      <div>
        <Button onClick={this.btnSortPriorityClick.bind(this)}>Set Sort Priority {(this.state.sortPriorityUP ? "UP" : "DOWN" )}</Button>
      </div>
    if(this.state.viewtype === "table"){
      return (
        <div>
        {btnChangeView}
        {TableView(this.taskData)}
        {btnSortTable}
        </div>
      );
    }else if (this.state.viewtype === "scrum") {
      return(
        <div>
        {btnChangeView}
        {ScrumView(this.taskData)}
        </div>
      );
    }else if(this.state.viewtype === "short") {
      return(
        <div>
        {btnChangeView}
        {TableShortView(this.taskData)}
        {btnSortTable}
        </div>
      );
    }else if(this.state.viewtype === "single"){
      let singleItem;
      if(this.state.taskId != null){
        //let singleItem = this.taskData.filter(function(id){return id === this.state.taskId});
  /*      for (let i = 0; i < this.taskData.length; ++i){
          if(this.taskData[i].id === this.state.taskId){
            console.log(i);
            console.log(this.taskData[i].id);
            this.SingleTaskItem = this.taskData[i];
            break;
          }
        }
*/
        return(
          <div>
            {btnChangeView}
          //  {SingleTaskView(this.SingleTaskItem)}
          
          {SingleTaskView(this.taskData[2])}
          </div>
        )
      }else {
        return (<div>task id is empty</div>)
      }
    }else{
      return(<div>non view</div>)
    }

  }
  btnChangeViewClick = viewtype => this.setState({viewtype : viewtype});
  //btnSortPriorityClick = () => this.taskData.sort( (itemA,itemB)=>itemA-itemB)
  btnSortPriorityClick = function () {
    if(this.state.sortPriorityUP){
      this.taskData.sort( (itemA,itemB)=>itemB.priority-itemA.priority);
      this.state.sortPriorityUP = false;
    }else{
      this.taskData.sort( (itemA,itemB)=>itemA.priority-itemB.priority);
      this.state.sortPriorityUP = true;
    }
    this.forceUpdate();
  }

}
