import React, { Component } from 'react';
import './LoginFormStyle.css';

export default class LoginForm extends Component {

  render(){
    return(
      <form action="" method="get" className="login_form">
        <p>Sign in</p>
        <label for="input_login">Login:</label>
        <input type="text" name="input_login" id="input_login"/>
        <br/>
        <label for="input_password">Pasword:</label>
        <input type="password" name="input_password" id="input_password"/>
        <br/>
        <input type="submit" value="login" className="login_form__button"/>
      </form>
    );
  }
}
